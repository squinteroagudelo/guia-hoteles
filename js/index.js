$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function(e){
        console.log('Cargando formulario de contacto...');
        $('#btn-contacto').removeClass('btn-warning');
        $('#btn-contacto').addClass('btn-outline-warning');
        $('#btn-contacto').addClass('text-muted');
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('Escribe tus dudas o comentarios!');
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('Regresando al inicio...');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('Selecciona un hotel!');
        $('#btn-contacto').removeClass('btn-outline-warning');
        $('#btn-contacto').addClass('btn-warning');
        $('#btn-contacto').removeClass('text-muted');
    });

});
